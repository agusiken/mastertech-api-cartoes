package br.com.apiprojetocartoes.services;

import br.com.apiprojetocartoes.models.Cartao;
import br.com.apiprojetocartoes.models.Pagamento;
import br.com.apiprojetocartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {
    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;

    public Pagamento setPagamento(Pagamento pagamento) {
        Cartao cartaoObjeto = cartaoService.getCartaoById(pagamento.getCartao().getNumeroCartao());
        pagamento.getCartao().setAtivo(cartaoObjeto.isAtivo());

        if (cartaoObjeto.isAtivo()) {
            Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
            return pagamentoObjeto;
        }

        throw new RuntimeException("Seu cartão não está ativo. Favor contatar a Central de Atendimento.");
    }

    public Iterable<Pagamento> getPagamentosPorIdCartao(Long idCartao) {
        Iterable<Pagamento> pagamentos = pagamentoRepository.findAllByCartaoIdCartao(idCartao);
        return pagamentos;
    }
}
