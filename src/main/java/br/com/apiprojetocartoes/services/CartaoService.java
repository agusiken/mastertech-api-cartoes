package br.com.apiprojetocartoes.services;

import br.com.apiprojetocartoes.models.Cartao;
import br.com.apiprojetocartoes.models.Cliente;
import br.com.apiprojetocartoes.repositories.CartaoRepository;
import br.com.apiprojetocartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {
    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;

    public Iterable<Cartao> getCartoes(){
        return cartaoRepository.findAll();
    }

    public Cartao setCartao(Cartao cartao){
        Cliente client = clienteService.buscarPorId(cartao.getCliente().getIdCliente());

        cartao.setCliente(client);

        Optional<Cartao> cartaoNum = cartaoRepository.findById(cartao.getNumeroCartao());

        if (cartaoNum.isPresent()) {
            return cartaoNum.get();
        }
        return cartaoRepository.save(cartao);
    }

    public Cartao getCartaoById(String numeroCartao) {
        Optional<Cartao> idCartao = cartaoRepository.findById(numeroCartao);

        if(idCartao.isPresent()){
            return idCartao.get();
        }

        throw new RuntimeException("Cartão não encontrado.");
    }

    public Cartao mudarStatusCartao(String numeroCartao, Cartao cartao) {

        if (cartaoRepository.existsByNumeroCartao(numeroCartao)) {
            Optional<Cartao> cartaoOptional = cartaoRepository.findByNumeroCartao(numeroCartao);
            cartao.setNumeroCartao(numeroCartao);
            cartao.setCliente(cartaoOptional.get().getCliente());
            Cartao cartaoObjeto = setCartao(cartao);

            return cartaoObjeto;
        }

        throw new RuntimeException("Cartão não encontrado.");

    }
}
