package br.com.apiprojetocartoes.services;

import br.com.apiprojetocartoes.models.Cliente;
import br.com.apiprojetocartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente setCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Iterable<Cliente> getClientes() {
        return clienteRepository.findAll();
    }

    public Cliente buscarPorId(Long idCliente) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(idCliente);

        if (clienteOptional.isPresent()) {
            return clienteOptional.get();
        }

        throw new RuntimeException("O cliente não foi encontrada!");
    }

    public Cliente getCliente(Long idCliente){
        Optional<Cliente> clienteOptional = clienteRepository.findById(idCliente);

        if(clienteOptional.isPresent()){
            return clienteOptional.get();
        }

        return null;
    }
}
