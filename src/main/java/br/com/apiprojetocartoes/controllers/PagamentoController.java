package br.com.apiprojetocartoes.controllers;

import br.com.apiprojetocartoes.models.Pagamento;
import br.com.apiprojetocartoes.models.dtos.PagamentoDTOPost;
import br.com.apiprojetocartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    public ResponseEntity<Pagamento> postPagamento(@RequestBody PagamentoDTOPost pagamentoDTOPost) {
        Pagamento pagamento = new Pagamento();

        pagamento.setCartao(pagamentoDTOPost.getCartao());
        pagamento.setDescricao(pagamentoDTOPost.getDescricao());
        pagamento.setValor(pagamentoDTOPost.getValor());

        Pagamento pagamentoObjeto = pagamentoService.setPagamento(pagamento);

        return ResponseEntity.status(201).body(pagamentoObjeto);
    }

    @GetMapping("/{idCartao}")
    public Iterable<Pagamento> getPagamentosByIdCartao(@PathVariable Long idCartao) {
        Iterable<Pagamento> pagamentos = pagamentoService.getPagamentosPorIdCartao(idCartao);
        return pagamentos;
    }
}
