package br.com.apiprojetocartoes.controllers;

import br.com.apiprojetocartoes.models.Cartao;
import br.com.apiprojetocartoes.models.dtos.CartaoDTOPatch;
import br.com.apiprojetocartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    public ResponseEntity<Cartao> setCartao(@RequestBody Cartao cartao) {

        Cartao cartaoObjeto = new Cartao();

        cartaoObjeto = cartaoService.setCartao(cartao);

        return ResponseEntity.status(201).body(cartao);
    }


    @GetMapping
    public Iterable<Cartao> getCartoes() {
        return cartaoService.getCartoes();
    }

    @GetMapping("/{numeroCartao}")
    public Cartao getCartao(@PathVariable String numeroCartao) {
        try {

            Cartao retorno = cartaoService.getCartaoById(numeroCartao);
            return retorno;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    //Mudar status de cartao
    @PatchMapping("/{numeroCartao}")
    public Cartao mudarStatusCartao(@PathVariable String numeroCartao, @RequestBody CartaoDTOPatch cartaoDTOPatch) {
        Cartao statusCartao = new Cartao();
        statusCartao.setAtivo(cartaoDTOPatch.isAtivo());

        try {
            Cartao retorno = cartaoService.mudarStatusCartao(numeroCartao, statusCartao);
            return retorno;

        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
