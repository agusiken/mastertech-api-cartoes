package br.com.apiprojetocartoes.controllers;

import br.com.apiprojetocartoes.models.Cliente;
import br.com.apiprojetocartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Cliente> setCliente(@RequestBody Cliente cliente) {
        Cliente clienteObjeto = clienteService.setCliente(cliente);

        return ResponseEntity.status(201).body(clienteObjeto);
    }

    @GetMapping
    public Iterable<Cliente> getClientes() {
        return clienteService.getClientes();
    }

    @GetMapping("/{idCliente}")
    public Cliente getCliente(@PathVariable(name = "idCliente") Long idCliente) {
        Cliente cliente = clienteService.buscarPorId(idCliente);
        return cliente;
    }
}
