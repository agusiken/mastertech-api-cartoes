package br.com.apiprojetocartoes.repositories;

import br.com.apiprojetocartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

    Iterable<Pagamento> findAllByCartaoIdCartao(long cartaoId);


}