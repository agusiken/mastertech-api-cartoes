package br.com.apiprojetocartoes.repositories;

import br.com.apiprojetocartoes.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, String> {
    Optional<Cartao> findByNumeroCartao(String numero);
    boolean existsByNumeroCartao(String numero);
}

