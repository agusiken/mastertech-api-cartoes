package br.com.apiprojetocartoes.repositories;

import br.com.apiprojetocartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
}
