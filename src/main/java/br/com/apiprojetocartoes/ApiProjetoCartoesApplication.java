package br.com.apiprojetocartoes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiProjetoCartoesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiProjetoCartoesApplication.class, args);
	}

}
