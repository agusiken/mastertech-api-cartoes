package br.com.apiprojetocartoes.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idCliente;

    @NotBlank(message = "O campo nome não pode ser vazio")
    @NotNull(message = "O campo não pode ser nulo")
    private String nomeCompleto;

    public Cliente() {
    }

    public Cliente(long idCliente, String nomeCompleto) {
        this.idCliente = idCliente;
        this.nomeCompleto = nomeCompleto;
    }

    public long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(long idCliente) {
        this.idCliente = idCliente;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }
}
