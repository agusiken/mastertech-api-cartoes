package br.com.apiprojetocartoes.models.dtos;

import br.com.apiprojetocartoes.models.Cliente;

public class CartaoDTOPost {
    private String numeroCartao;

    private Cliente cliente;

    public CartaoDTOPost() {
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
