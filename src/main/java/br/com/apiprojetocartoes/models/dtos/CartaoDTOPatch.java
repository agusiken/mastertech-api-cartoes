package br.com.apiprojetocartoes.models.dtos;

public class CartaoDTOPatch {
    private boolean isAtivo;

    public CartaoDTOPatch() {
    }

    public boolean isAtivo() {
        return isAtivo;
    }

    public void setAtivo(boolean ativo) {
        this.isAtivo = isAtivo;
    }

}
