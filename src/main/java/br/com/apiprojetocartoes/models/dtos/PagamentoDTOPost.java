package br.com.apiprojetocartoes.models.dtos;

import br.com.apiprojetocartoes.models.Cartao;

public class PagamentoDTOPost {

    private Cartao cartao;

    private String descricao;

    private double valor;

    public PagamentoDTOPost(Cartao cartao, String descricao, double valor) {
        this.cartao = cartao;
        this.descricao = descricao;
        this.valor = valor;
    }

    public PagamentoDTOPost() {
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
